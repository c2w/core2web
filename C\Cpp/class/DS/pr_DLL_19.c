#include<stdio.h>
#include<stdlib.h>

void* malloc(size_t);
void free(void *);

typedef struct node
{
    struct node *prev;

    int data;

    struct node *next;

}node;

node *head =NULL;

node* createnode(){

    node *newnode=(node*)malloc(sizeof(node));
    newnode->prev=NULL;

    printf("Enter Data : \n");
    scanf("%d",&newnode->data);

    newnode->next=NULL;

    return newnode;


}

void addnode(){

    node *newnode=createnode();

    if(head==NULL){

        head=newnode;
    }else{

        node *temp=head;

        while(temp->next!=NULL){

            temp=temp->next;

        }
        temp->next=newnode;
        newnode->prev=temp;

    }
}

void addfirst(){

    node *newnode=createnode();

    if(head==NULL){

        head=newnode;

    }else{

        newnode->next=head;
        head->prev=newnode;
        head=newnode;
    }
}

int countnode(){

    node *temp=head;

	int count=0;

	while(temp!=NULL){

		count++;

		temp=temp->next;

	}
	return count;
}

int addAtpos(int pos){

	int count =countnode();

	if(pos<=0||pos>=count+2){

		printf("Invalid node position \n");
		return -1;
    }
    else{

        if(pos==count+1){

            addnode();

        }else{

            if(pos==1){

                addfirst();

            }else{

                node *newnode=createnode();
                node *temp=head;

                while(pos-2){

                    temp=temp->next;
                    pos--;

                }

                newnode->next=temp->next;
                newnode->prev=temp;

                temp->next=newnode;
                temp->next=newnode;

            }
            return 0;
        }
    }
}

int dellast(){

    int count=countnode();

    if(head==NULL){

        printf("Linklist is Empty \n");
        return -1;

    }else{

        if(count==1){

            free(head);
            head=NULL;

        }else{

            node *temp=head;

            while(temp->next->next!=NULL){

                temp=temp->next;

            }

            free(temp->next);
            temp->next=NULL;

        }
        return 0;
    }
}

int delfirst(){

    int count=countnode();

    if(head==NULL){

        printf("Nothing node to delete \n");
        return -1;

    }else{

        if(count==1){

            free(head);
            head=NULL;

        }else{

            head=head->next;
            free(head->prev);
            head->prev=NULL;

        }
    }

}

int delAtpos(int pos){

    int count=countnode();

    if(pos<=0||pos>count){

        printf("Wrong Node position to delete \n");
        return -1;

    }else{

        if(pos==1){

            delfirst();

        }else{

            if(pos==count){

                dellast();

            }else{

                struct node *temp=head;

                while(pos-2){

                    temp=temp->next;
                    pos--;

                }

                temp->next=temp->next->next;
                free(temp->next->prev);
                temp->next->prev=temp;

            }

        }
        return 0;

    }
}

int printDLL(){

    if(head==NULL){

        printf("Empty Linked List \n");
        return -1;

    }else{

        node *temp=head;
        while(temp->next!=NULL){

            printf("|%d|->",temp->data);
            temp=temp->next;

        }

        printf("|%d|\n",temp->data);
        return 0;
    }
}

void main(){

    int countnode;

    printf("Enter node count : \n");
    scanf("%d",&countnode);

    for(int i=1;i<=countnode;i++){
        addnode();
    }

    char choice;

	do{
		printf("1.Addlast\n");
		printf("2.addfirst\n");
		printf("3.addAtpos\n");
		printf("4.printll\n");
		printf("5.delfirst\n");
		printf("6.dellast\n");
        printf("7.delAtpos\n");
		

		int ch;

		printf("Enter Choice :\n");
		scanf("%d",&ch);

		switch (ch){

			case 1:
			       addnode();
			       break;

			case 2:
			       addfirst();
			       break;

			case 3:
			       { 
				int pos;
				printf("Enter position to add node : \n");
                                scanf("%d",&pos);
				addAtpos(pos);
			       }
			       break;

			case 4:
			       printDLL();
			       break;

			case 5:

			       delfirst();
			       break;

			case 6:
			      
			      dellast();
			      break;
            
            case 7:
                   {

                int pos;
				printf("Enter position to add node : \n");
                scanf("%d",&pos);
                   delAtpos(pos);
        }
                   break;

			default :
			         printf("Wrong choice \n");

		}

		getchar();
		printf("Do you want to continue y/n? \n");
		scanf("%c",&choice);

	}
	while(choice=='y'||choice=='Y');
}