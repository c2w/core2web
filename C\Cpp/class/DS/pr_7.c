#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct country{

	char cname[20];
	int population;
	float GDP;

};

void main(){

	struct country *cptr=(struct country *)malloc(sizeof(struct country));

	strcpy(cptr->cname,"INDIA");
	cptr->population=135;
	cptr->GDP=7.5;

	printf("%s\n",cptr->cname);
	printf("%d\n",cptr->population);
	printf("%f\n",cptr->GDP);
}
