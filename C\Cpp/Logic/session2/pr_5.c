#include<stdio.h>

void main(){

	int row;

	printf("Enter number of rows\n");
	scanf("%d",&row);

	char ch1='a';
	char ch2='A';

	for(int i=0;i<row;i++){

		for(int j=0;j<row;j++){

			if(j%2!=0){

				printf("%c ",ch2);
			}else{

				printf("%c ",ch1);
			}
			ch1++;
			ch2++;
		}
		printf("\n");
	}
}
