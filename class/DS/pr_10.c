#include<stdio.h>
#include<stdlib.h>


struct node{

	int data;
	struct node *next;

};

void main(){

	struct node* head =NULL;

	//First node
	
	struct node *newnode=(struct node*)malloc(sizeof(struct node));

	newnode->data=10;
	newnode->next=NULL;

	// connecting First node 
	
	head =newnode;

	// second node 
	
	newnode=(struct node*)malloc(sizeof (struct node));
	newnode->data=20;
	newnode->next=NULL;

	//connecting second node
	
	head->next=newnode;

	//thrid node 
	
	newnode=(struct node*)malloc(sizeof (struct node));
        newnode->data=30;
        newnode->next=NULL;

	// connecting thrid node
	head->next->next=newnode;

	struct node *temp=head;

	while(temp!=NULL){

		printf("%d  ",temp->data);

		temp=temp->next;
	}
	printf("\n");

}




