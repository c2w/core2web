#include<stdio.h>
#include<stdlib.h>

struct demo{

	int data;

	struct demo *next;
};

struct demo *head =NULL;

struct demo* createnode(){

	struct demo *newnode=(struct demo*)malloc(sizeof(struct demo));

	printf("Enter data : \n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;

}

void addnode(){

	struct demo *newnode=createnode();

	if(head==NULL){

		head =newnode;

	}else{
		struct demo *temp=head;

		while(temp->next!=NULL){

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void addfirst(){

struct demo *newnode=createnode();

if(head==NULL){

	head =newnode;
}else{
	newnode->next=head;
	head=newnode;
}
}

void printll(){

	struct demo *temp=head;

	while(temp!=NULL){

		printf("|%d|",temp->data);

		temp=temp->next;
	}
}

void addAtpos(int pos){

	struct demo *newnode=createnode();
	struct demo  *temp=head;

	while(pos-2){

		temp=temp->next;
		pos--;
	}
	newnode->next=temp->next;
	temp->next=newnode;
}

void main(){

	int countnode;

	printf("Enter no of node count to add \n");
	scanf("%d",&countnode);

	for(int i=0;i<countnode;i++){

		addnode();
	}

	printll();

	addfirst();

	printll();

	int pos;

	printf("Enter position to add node : \n");
	scanf("%d",&pos);

	addAtpos(pos);

	printll();

}
	

